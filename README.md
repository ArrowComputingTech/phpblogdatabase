This php program is the backend (and command-line frontend) of a blog platform.
It performs CRUD operations on a mysql database via PDO.

It is the final exercise from Section 22: Complete PHP OOP in the "Complete PHP Full Stack Web Developer" course on udemy, though it has been heavily modified to run from the command line with a user menu.

To run this program, you will need a mysql database, user, and table.

The mysql configuration can be changed in config/config.php
