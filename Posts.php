<?php

  require_once('Database.php');

  class Posts {
    private $db;
    
    public function __construct() {
      $this->db = new Database();
    }

    // Get count of posts 
    public function getPostCount() {
      $this->db->query("SELECT COUNT(*) FROM " . $this->db->getTableName());
      $rs = $this->db->resultset();
      foreach ($rs[0] as $k => $v) {
        $postCount = $v;
      }
      return $postCount;
    }

    //Get IDs of posts
    public function getIDs() {
      $ids = [];
      $this->db->query("SELECT id FROM " . $this->db->getTableName());
      $rs = $this->db->resultset();
      foreach ($rs as $k => $v) {
        foreach ($v as $l => $w) {
          array_push($ids, $w);
        }
      }
      return $ids;
    }

    // Get all posts
    public function getPosts() {
      $this->db->query("SELECT * FROM " . $this->db->getTableName());
      return $this->db->resultset();
    }

    // Get one post
    public function getPostById($id) {
      $this->db->query("SELECT * FROM " . $this->db->getTableName() . " WHERE id = :id");
      $this->db->bind(':id', $id);
      return $this->db->single();
    }

    // Insert new post
    public function addPost($data) {
      $this->db->query("INSERT INTO " . $this->db->getTableName() . " (title, content) VALUES(:title, :content)");
      $this->db->bind(':title', $data['title']);
      $this->db->bind(':content', $data['content']);
      if ($this->db->execute()) {
        echo "Created new post!" . PHP_EOL;
        return true;
      } else {
        echo "Failed to create new post...." . PHP_EOL;
        return false;
      }
    }

    // Update post
    public function updatePost($data) {
      $this->db->query("UPDATE " . $this->db->getTableName() . " SET title = :title, content = :content WHERE id = :id");
      $this->db->bind(':id', $data['id']);
      $this->db->bind(':title', $data['title']);
      $this->db->bind(':content', $data['content']);
      if ($this->db->execute()) {
        return true;
      } else {
        return false;
      }
    }

    // Delete post
    public function destroyPost($id) {
      $this->db->query("DELETE FROM " . $this->db->getTableName() . " WHERE id = :id");
      $this->db->bind(':id', $id);
      if ($this->db->execute()) {
        return true;
      } else {
        return false;
      }
    }
  }

?>
