<?php

  require_once('Posts.php');

  $p = new Posts();
  $menu = "";

  while ($menu != "Q") {
    echo "What would you like to do?" . PHP_EOL;
    echo "(W)rite new post" . PHP_EOL;
    echo "(V)iew posts" . PHP_EOL;
    echo "(U)pdate a post" . PHP_EOL;
    echo "(D)elete a post" . PHP_EOL;
    echo "(C)ount number of posts" . PHP_EOL;
    echo "(Q)uit" . PHP_EOL;
    $menu = strtoupper(readline("? "));
    switch ($menu) {
      case "W":
        createNewPost($p);
        break;
      case "V":
        viewAllPosts($p);
        break;
      case "U":
        updatePost($p);
        break;
      case "D":
        deletePost($p);
        break;
      case "C":
        echo "Total number of posts: " . getPostsCount($p) . PHP_EOL;
        break;
      case "Q":
        break;
      default:
        echo "Invalid key pressed. Try again." . PHP_EOL;
    }
  }

  // Insert new post
  function createNewPost($p) {
    $title = readline("Enter a title for the new post: " . PHP_EOL);
    $content = readline("Now write the post: " . PHP_EOL);
    $data = ['title'=>$title, 'content'=>$content];
    $p->addPost($data);
  }

  // Update post
  function updatePost($p) {
    if (getPostsCount($p) == '0') {
      echo "No posts exist. You must create a post first." . PHP_EOL;
      return;
    }
    echo ("Which post would you like to edit? Valid choices: ");
    $ids = getPostIDs($p);
    foreach ($ids as $id) {
      if ($id != end($ids)) {
        echo ($id . ", ");
      } else {
        echo ($id . ". ");
      }
    }
    echo PHP_EOL;
    $updateID = readline();
    $pst = viewOnePost($p, $updateID);
    foreach ($pst as $k => $v) {
      if ($k == 'title') {
        $preTitle = $v;
      }
      if ($k == 'content') {
        $preContent = $v;
      }
    }
    echo ("Current title: " . $preTitle . PHP_EOL);
    $title = readline("Enter a new post title or press enter to keep it the same: ");
    echo ("Current content: " . $preContent . PHP_EOL);
    $content = readline("Enter new post content or press enter to keep it the same: " . PHP_EOL);
    if ($title == "") {
      $title = $preTitle;
    }
    if ($content == "") {
      $content = $preContent;
    }
    $data = ['id'=>$updateID, 'title'=>$title, 'content'=>$content];
    $p->updatePost($data);
    echo "Post updated!" . PHP_EOL;
  }

  function deletePost($p) {
    if (getPostsCount($p) == '0') {
      echo "No posts exist. You must create a post first." . PHP_EOL;
      return;
    }
    echo ("Which post would you like to delete? Valid choices: ");
    $ids = getPostIDs($p);
    foreach ($ids as $id) {
      if ($id != end($ids)) {
        echo ($id . ", ");
      } else {
        echo ($id . ". ");
      }
    }
    echo PHP_EOL;
    $deleteID = readline();
    $pst = viewOnePost($p, $deleteID);
    foreach ($pst as $k => $v) {
      if ($k == 'title') {
        $title = $v;
      }
      if ($k == 'content') {
        $content = $v;
      }
    }
    echo ("Post title: " . $title . PHP_EOL);
    echo ("Post content: " . $content . PHP_EOL);
    $choice = strtoupper(readline("Are you sure you want to delete this post? (Y/N)"));
    if ($choice == "Y") {
      $p->destroyPost($deleteID);
      echo "Post deleted." . PHP_EOL;
    } else {
      echo "Nothing has been changed." . PHP_EOL;
    }
  }

  function viewOnePost($p, $id) {
    return $p->getPostById($id);
  }

  function viewAllPosts($p) {
    if (getPostsCount($p) == '0') {
      echo "No posts exist. You must create a post first." . PHP_EOL;
      return;
    }
    var_dump($p->getPosts());
  }

  function getPostsCount($p) {
    return $p->getPostCount();
  }

  function getPostIDs($p) {
    return $p->getIDs();
  }

?>
